package a02a.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class MultiQueueImpl<T, Q> implements MultiQueue<T, Q> {

    private Map<Q,List<T>> queues = new HashMap<>(); 
    
    @Override
    public Set<Q> availableQueues() {
       return queues.keySet();
    }

    @Override
    public void openNewQueue(Q queue) {
        queues.put(queue, new LinkedList<>());
    }

    @Override
    public boolean isQueueEmpty(Q queue) {
        return this.queues.get(queue).isEmpty();
    }

    @Override
    public void enqueue(T elem, Q queue) {
        this.queues.get(queue).add(elem);     
    }

    @Override
    public Optional<T> dequeue(Q queue) {
       return queues.get(queue).isEmpty() ? Optional.empty() : Optional.of(queues.get(queue).remove(0));
    }

    @Override
    public Map<Q, Optional<T>> dequeueOneFromAllQueues() {
        Map<Q,Optional<T>> map = new HashMap<>();
        for(Q q : queues.keySet()) {
            map.put(q, dequeue(q));
        }
        return map;
    }

    @Override
    public Set<T> allEnqueuedElements() {
       Set<T> set = new HashSet<>();
       for(Q q : queues.keySet()) {
           set.addAll(queues.get(q));
       }
       return set;
    }

    @Override
    public List<T> dequeueAllFromQueue(Q queue) {
        List<T> res = new LinkedList<>(queues.get(queue));
        queues.get(queue).clear();        
        return res;
    }

    @Override
    public void closeQueueAndReallocate(Q queue) {
        // TODO Auto-generated method stub
        
    }

}
