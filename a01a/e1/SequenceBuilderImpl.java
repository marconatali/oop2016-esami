package a01a.e1;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class SequenceBuilderImpl <X> implements SequenceBuilder<X> {
    
    private List<X> list = new LinkedList<>();
    boolean build=false;

    @Override
    public void addElement(X x) {
       list.add(x);
    }

    @Override
    public void removeElement(int position) {
        list.remove(position);
    }

    @Override
    public void reverse() {
        Collections.reverse(list);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Optional<Sequence<X>> build() {        
        if(build) {
            return Optional.empty();
        }
        
        build = true;

       Sequence<X> res = new Sequence<X>() {

        @Override
        public Optional<X> getAtPosition(int position) {
           return this.size() <= position ? Optional.empty() : Optional.of(list.get(position));
        }

        @Override
        public int size() {
            return list.size();
        }

        @Override
        public List<X> asList() {
           return list;
        }

        @Override
        public void executeOnAllElements(Executor<X> executor) {
           list.stream().forEach(x->executor.execute(x));
        }
    };
    
    return Optional.of(res);
    }

    @Override
    public Optional<Sequence<X>> buildWithFilter(Filter<X> filter) {
       return list.stream().filter(x->!filter.check(x)).findFirst().isPresent() ? Optional.empty() : this.build();
    }

    @Override
    public <Y> SequenceBuilder<Y> mapToNewBuilder(Mapper<X, Y> mapper) {
        final SequenceBuilder<Y> builder = new SequenceBuilderImpl<>();
        this.list.forEach(x -> builder.addElement(mapper.transform(x)));
        return builder;
    }

	
}
