package a04.e1;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class BitIteratorsFactoryImpl implements BitIteratorsFactory {

	@Override
	public Iterator<Bit> empty() {
		return Stream.of(Bit.ZERO).limit(0).iterator();
	}

	@Override
	public Iterator<Bit> zeros() {
		return Stream.generate(()->Bit.ZERO).iterator();
	}

	@Override
	public Iterator<Bit> ones() {
		return Stream.generate(()->Bit.ONE).iterator();
	}

	@Override
	public Iterator<Bit> fromByteStartingWithLSB(int b) {
		return Stream.iterate(b, e->e/2).map(e->(e%2)==0 ? Bit.ZERO : Bit.ONE).limit(8).iterator();
	}

	@Override
	public Iterator<Bit> fromBitList(List<Bit> list) {
		return list.iterator();
	}

	@Override
	public Iterator<Bit> fromBooleanList(List<Boolean> list) {
		return list.stream().map(e->e ? Bit.ONE : Bit.ZERO).iterator();
	}

}
