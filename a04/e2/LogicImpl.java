package a04.e2;

import java.util.Collections;
import java.util.List;

public class LogicImpl implements Logic {

	public static final List<List<String>> list = List.of(Collections.nCopies(5, "l"),
	Collections.nCopies(3, "r"),
	List.of("l","r","l","r"));
	private List<String> currentSequence = List.of();
	private int currentPos = 0;
	
	
	@Override
	public void hit(String s) {
		this.currentPos=this.currentSequence.get(currentPos).equals(s) ? this.currentPos+1 : 0;			
	}

	@Override
	public void select(List<String> sequence) {
		this.currentSequence=sequence;
	}

	@Override
	public boolean isOver() {
		return this.currentPos==this.currentSequence.size();
	}

}
