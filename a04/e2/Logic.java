package a04.e2;

import java.util.List;

public interface Logic {
	void hit(String s);
	void select(List<String> sequence);
	boolean isOver();
}
