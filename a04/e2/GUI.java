package a04.e2;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;


public class GUI extends JFrame{
   
    private JComboBox<String> box;
    private final Logic logic;
    
    public GUI(){
        box = new JComboBox<>();
        box.addItem(LogicImpl.list.get(0).toString());
        box.addItem(LogicImpl.list.get(1).toString());
        box.addItem(LogicImpl.list.get(2).toString());
        
        logic = new LogicImpl();
              
        JPanel jp = new JPanel();
        jp.add(box);
        
        this.setSize(200, 100);
        this.getContentPane().add(jp);
       
        ActionListener ac = (e)->{
            JComboBox b = (JComboBox)e.getSource();
            logic.select(LogicImpl.list.get(b.getSelectedIndex()));
        };
        
        ActionListener al = (e)->{
        	 final JButton bt = (JButton)e.getSource();
        	 logic.hit(bt.getText());
        	 if(logic.isOver()) {
        		 System.exit(1);
        	 }
        };
        
               
        box.addActionListener(ac);
        
        JButton l = new JButton("l");
        l.addActionListener(al);
        jp.add(l);
        
        JButton r = new JButton("r");
        r.addActionListener(al);  
        jp.add(r);
        
        this.setVisible(true);
    }
  
}
