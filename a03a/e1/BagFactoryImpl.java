package a03a.e1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BagFactoryImpl implements BagFactory {

	@Override
	public <X> Bag<X> empty() {
		return this.fromList(new LinkedList<X>());
	}

	@Override
	public <X> Bag<X> fromSet(Set<X> set) {
		return this.fromList(set.stream().collect(Collectors.toList()));
	}

	@Override
	public <X> Bag<X> fromList(List<X> list) {
		return new Bag<X>() {

			@Override
			public int numberOfCopies(X x) {
				return (int) list.stream().filter(e->e.equals(x)).count();
			}

			@Override
			public int size() {
				return list.size();
			}

			@Override
			public List<X> toList() {
				return list;
			}
		};
	}

	@Override
	public <X> Bag<X> bySupplier(Supplier<X> supplier, int nElements, ToIntFunction<X> copies) {
		
		return null;
	}

	@Override
	public <X> Bag<X> byIteration(X first, UnaryOperator<X> next, int nElements, ToIntFunction<X> copies) {
		// TODO Auto-generated method stub
		return null;
	}

}
