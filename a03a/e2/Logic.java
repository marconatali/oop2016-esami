package a03a.e2;

public interface Logic {
	void hit(int button);
	boolean isOver();
	int getA();
	int getB();
}
