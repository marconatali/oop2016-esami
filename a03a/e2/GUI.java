package a03a.e2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

public class GUI extends JFrame{
    
	private final List<JButton> buttons;
	private Logic logic;
    	
    public GUI(int size){
    	
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*70,size*10);
        
        buttons=new LinkedList<JButton>();
        logic = new LogicImpl(size);
        
        
        JPanel panel = new JPanel();
        this.getContentPane().add(BorderLayout.WEST,panel);
        
        JPanel panelSet = new JPanel();
        this.getContentPane().add(BorderLayout.CENTER,panelSet);
        
        ActionListener alA = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit(0);
            if(!logic.isOver()) {
            	draw(size);
            } else {
            	System.exit(1);
            }            
        };
        
        ActionListener alB = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit(1);
            if(!logic.isOver()) {
            	draw(size);
            } else {
            	System.exit(1);
            }            
        };
        
        ActionListener alR = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic=new LogicImpl(size);   
            draw(size);
        };
        
        
        final JButton moveA = new JButton("Move A");
        moveA.addActionListener(alA);
        panelSet.add(moveA);
        
        final JButton moveB = new JButton("Move B");
        moveB.addActionListener(alB);
        panelSet.add(moveB);
        
        final JButton reset = new JButton("Reset");
        reset.addActionListener(alR);
        panelSet.add(reset);
                
        for (int i=0; i<size; i++){
            final JButton jb = new JButton(" ");
            //jb.addActionListener(al);
            this.buttons.add(jb);
            panel.add(jb);
            jb.setEnabled(false);
        }
        draw(size);
        this.setVisible(true);
    }

	private void draw(int size) {
		for(int i=0;i<size;i++) {
			if(i==logic.getA()) {
				buttons.get(i).setText("A");
			} else {
				if(i==logic.getB()) {
					buttons.get(i).setText("B");
				} else {
					buttons.get(i).setText(" ");
				}
			}
		}
	}
}
