package a03a.e2;

public class LogicImpl implements Logic {

	int posA;
	int posB;
	int size;
	
	public LogicImpl(int size) {
		this.posA=0;
		this.posB=1;
		this.size=size;
	}
	
	@Override
	public void hit(int button) {
		posA = button == 0 && posA+1<posB ? posA+1 : posA;
		posB = button == 1 && posB+1<size ? posB+1 : posB;
	}

	@Override
	public boolean isOver() {
		return posB==size-1 && posA==size-2;
	}

	@Override
	public int getA() {
		return posA;
	}
	
	public int getB() {
		return posB;
	}

}
