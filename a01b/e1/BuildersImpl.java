package a01b.e1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class BuildersImpl implements Builders {
	
	

	@Override
	public <X> ListBuilder<X> makeBasicBuilder() {
		return new ListBuilder<X>() {
			boolean built=false;
			private List<X> list = new ArrayList<X>();
			@Override
			public void addElement(X x) {
				checkState(!built);
				list.add(x);
				
			}
			@Override
			public List<X> build() {
				checkState(!built);
				built=true;
				return list;
			}
		};
	}

	@Override
	public <X> ListBuilder<X> makeBuilderWithSize(int size) {		
		return new ListBuilder<X>() {
			private List<X> list = new ArrayList<X>();
			@Override
			public void addElement(X x) {
				if(list.size()<size) {
					list.add(x);
				}
			}
			@Override
			public List<X> build() {
				checkState(list.size() == size);
				return list;
			}
		};
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElements(Collection<X> from) {
		return new ListBuilder<X>() {
			private List<X> list = new ArrayList<X>();
			@Override
			public void addElement(X x) {
				checkArgument(from.contains(x));
				list.add(x);
			}
			@Override
			public List<X> build() {
				return list;
			}
		};
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElementsAndWithSize(Collection<X> from, int size) {		
		
		return new ListBuilder<X>() {
			List<X> list = new ArrayList<X>(from);
			boolean built=false;
			@Override
			public void addElement(X x) {
				checkState(!built);
				checkArgument(from.contains(x));
				if(list.size()<size) {
					list.add(x);
				}				
			}
			@Override
			public List<X> build() {
				checkState(!built);
				checkState(list.size()==size);
				built = true;
				return this.list;
			}
		};
	}
	
	private void checkState (boolean b) {
		if(!b) {
			throw new IllegalStateException();
		}
	}
	
	private void checkArgument (boolean b) {
		if(!b) {
			throw new IllegalArgumentException();
		}
	}

}
